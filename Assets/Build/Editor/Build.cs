﻿using UnityEditor;
using UnityEditor.Build.Reporting;

namespace Build.Editor
{
    public static class Build
    {
        public static void WebGL()
        {
            PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Disabled;

            var report = BuildPipeline.BuildPlayer(
                new string[]{"Assets/Scenes/SampleScene.unity"},
                "dist/webgl",
                BuildTarget.WebGL,
                BuildOptions.None
            );

            if (report.summary.result != BuildResult.Succeeded)
            {
                EditorApplication.Exit(1);
            }
        }
    }
}